from rdflib import Graph, Namespace, Literal
from rdflib.plugins.sparql import prepareQuery
from pathlib import Path

# Find the test files
test_paths = [path for path in Path(".").glob("test_*.ttl")]
for test_index, test_path in enumerate(test_paths):
    test_index += 1
    # Load a test file
    print(f"Results for Test {test_index}: {test_path}")
    g = Graph()
    g.parse(test_path, format="turtle")

    # Define a namespace for convenience
    ex = Namespace("http://example.org/")

    # Read SPARQL query from file
    sparql_query_file_path = "../contributing_acts.rq"
    with open(sparql_query_file_path, "r") as query_file:
        sparql_query = query_file.read()

    # Prepare the SPARQL query
    query = prepareQuery(sparql_query)

    # Execute the query and print the results
    for result_index, row in enumerate(g.query(query)):
        result_index +=1
        print(f"Test {test_index}-{result_index}. [{row[0]}] contributes fact [{row[1]}] for the preconditions of [{row[2]}].")