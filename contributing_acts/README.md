# Contributing Acts

*"If I want to do [some Act] A2, what other acts can help me to satisfy A2's preconditions?"*
  
This query can help with planning and with visualizing timelines or flows of a procedure. More formally speaking, the query finds a "contributing Act" A1 for a "dependent Act" A2, where A1 creates a Fact which occurs in the preconditions of A2.

## Tests

Query results can be generated for a variety of cases in `/tests` by running `/tests.main.py` (requires the `rdflib` module).
The script runs on the `*.ttl` files but the original `*.json` files from the norm editor used to construct the cases are also included.

The cases are as follows:
1. A1 -> A2 (Act A1 contributes some linking fact to Act A2)
2. A1 -> A2 -> A3
3. A1 -> A2 (through a NOT-NOT construction)
4. [no dependencies] (because of a NOT-NOT-NOT construction)
5. A1 -> A2 **and** B1 -> B2
6. A1 -> A2 **and** B1 -> B2 (additionally an unrelated act C1 exists)
7. A1 -> A2 **and** B1 -> B2 **and** C1 -> C2 (these pairs respectively use the actor, object, and recipient fields to link acts rather than `flint:hasPrecondition`)