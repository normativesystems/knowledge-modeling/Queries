# Queries

FLINT models can be investigated using queries. Because we work with Semantic Web Technology, we use SPARQL queries.

The queries in this repository are:

- `/contributing_acts`: *"If I want to do [some Act] A2, what other acts can help me to satisfy A2's preconditions?"*  
This query can help with planning and with visualizing timelines or flows of a procedure. More formally speaking, the query finds a "contributing Act" A1 for a "dependent Act" A2, where A1 creates a Fact which occurs in the preconditions of A2.